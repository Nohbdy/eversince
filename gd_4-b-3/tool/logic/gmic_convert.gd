## G'MIC Convert
## A tool for accessing G'MIC externally via Godot(4.x)
## License: MD0-0.7 (gitlab.com/GPDMCC/MD0)

## @tool
## class_name
class_name MenuGeneric
## extends
extends Control
# Menu Generic
# doscstring test

## signals
## enums
## constants
## exported variables
## public variables
## private variables
## onready variables

## optional built-in virtual _init method
#func _init() -> void:
#	pass


## built-in virtual _ready method
func _ready() -> void:
	pass


## remaining built-in virtual methods
func _process(delta: float) -> void:
	pass


## public methods
#func public_method() -> void:
#	pass


## private methods
#func _on_some_signal() -> void:
#	pass
