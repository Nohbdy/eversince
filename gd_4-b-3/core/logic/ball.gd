# BALL  ( )
extends RigidBody2D

@onready @export var speed: float = 500
@onready var position_start := position


func bounce() -> void:
	rotate(PI*2)


func collide() -> bool:
	if get_contact_count() > 0:
		return true
	else:
		return false


func _ready() -> void:
	pass


#PROCESS
func _process(delta: float) -> void:
	if collide():
		bounce()
		move_and_collide(linear_velocity)


func _draw() -> void:
	pass # i am going to make a liletlie doodad thingy line drawy
	# thang, thing . yawwlkhhhhgkfghfxjjjjjjjjjjj
