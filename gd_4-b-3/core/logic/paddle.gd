# PADDLE <===>
extends CharacterBody2D

@onready @export var speed: float = 50

func _ready() -> void:
	pass

#PROCESS
func _process(delta: float) -> void:
	# Movement
	if Input.is_action_pressed("ui_right"): # Right
		velocity.x = speed
	elif Input.is_action_pressed("ui_left"): # Left
		velocity.x = -speed
	else:
		velocity.x = 0
	
	if Input.is_action_pressed("ui_up"): # Up
		velocity.y = -speed
	elif Input.is_action_pressed("ui_down"): # Down
		velocity.y = speed
	else:
		velocity.y = 0
	
	move_and_collide(velocity)

# PHYS PROCESS

