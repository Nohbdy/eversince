## EverSince by Nohbdy Ahtall - gitlab.com/Nohbdy/EverSince
## https://docs.godotengine.org/en/latest/tutorials/scripting/gdscript/gdscript_documentation_comments.html?highlight=bbcode#bbcode-and-class-reference

extends Node
## # docstring
# doscstring test

## signals

## enums

## constants

## exported variables

## public variables

## private variables

## onready variables

## optional built-in virtual _init method

## built-in virtual _ready method
func _ready() -> void:
	print("Scene: ", name, " readied.")
	

## remaining built-in virtual methods
func _process(delta: float) -> void:
	pass

## public methods

## private methods
